angular
  .controller('psembedCtrl', loadFunction);

loadFunction.$inject = ['$scope', 'structureService', 'storageService', '$location'];

function loadFunction($scope, structureService, storageService, $location) {
  //Register upper level modules
  structureService.registerModule($location, $scope, 'psembed');

  setTimeout(function () {
    structureService.launchSpinner('.transitionloader');
  }, 100);
}
