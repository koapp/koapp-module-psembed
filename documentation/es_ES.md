# **(How to) Configurar el Embed Module**

  

Si bien no es especialmente recomendable su uso (a los markets y especialmente a **Apple**, no les acaba de gustar este formato) hay ocasiones en que no queda más remedio que **embeber una página web en una App**. Para estos casos, en King of App hemos desarrollado este módulo de rápida y fácil configuración.

  

Al tratarse de un contenido embebido sólo hay que rellenar el campo habilitado con **la URL de destino**. Nada más y nada menos. Nosotros nos encargamos de que salga en pantalla cogiendo los **parámetros responsive** de la web.

  

En ocaciones querras mantener una versión web mobile de tu web además de incluir parte de su contenido en una app. Para esos escenarios hemos desarrollado un plugin de wordpress que te permite disponer de una tercera versión de tu web con css personalizado. De ésta forma es posible disponer de tu web mobile además de una version app con contenido adicional.

Para usarlo solo debes instalar el plugin:

https://s3.eu-central-1.amazonaws.com/kingofapp.com/koaembed.zip

Una vez activado puedes configurarlo desde el menú **Ajustes/Koa Embed**

Para utilizarlo agrega **[[Query param]]=true** al final de la url en la configuración de tu módulo. *No olvides reemplazar  [[Query param]] con el valor que has configurado en tu WordPress*

  

Lo dicho, si bien este módulo te puede ayudar en múltiples ocasiones, como todo gran poder, hay que saber utilizarlo con moderación y siempre combinado con otros módulos de contenido.

  

**En caso de tener problemas con el scroll en ios:**

  

Añadir la siguiente linea en el css de tu página web:

  

```@media screen and (min-width: 250px) and (max-width: 1024px) {html, body {height: 100% !important;overflow-y: scroll !important;-webkit-overflow-scrolling: touch !important;}}```